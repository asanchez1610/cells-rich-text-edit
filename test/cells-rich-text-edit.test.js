import {
  html, fixture, assert, fixtureCleanup,
} from '@open-wc/testing';
import '../cells-rich-text-edit.js';

suite('CellsRichTextEdit', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-rich-text-edit></cells-rich-text-edit>`);
    await el.updateComplete;
  });

  test('Test mode full', () => {
    el.mode = 'full';
  });

  test('Test prop incluir comandos', () => {
    el.incluir = ['bgcolor'];
  });

  test('Test prop excluir comandos', () => {
    el.excluir = ['bgcolor'];
  });

  test('Test function onClickLink', () => {
    const evt = {
      preventDefault: () => {},
      stopPropagation: () => {},
      target: {
        dataset: {
          link: '#link',
        },
      },
    };
    el.onClickLink(evt);
  });

  test('Test function verifyLinks', () => {
    const editor = el.shadowRoot.querySelector('main > .content-editor');
    const a = document.createElement('a');
    editor.appendChild(a);
    el.verifyLinks();
  });

  test('Test function formatSelected', () => {
    const evt = {
      preventDefault: () => {},
    };
    el.formatSelected(evt, 'bold');
    el.formatSelected(evt, 'createlink');
  });

  test('Test function getContent', () => {
    el.setContent('<div>Test</div>');
    const content = el.getContent();
    assert.isTrue(content.length > 0);
  });

  test('Test function contentHTML', () => {
    el.setContent('<div>Test</div>');
    el.contentHTML();
  });

  test('Test function addLinkElement', () => {
    el.shadowRoot.querySelector('#input-link').value = '#';
    const a = document.createElement('a');
    const text = document.createTextNode('Text');
    a.appendChild(text);
    a.setAttribute('data-link', 'link');
    const evt = {
      preventDefault: () => {},
      stopPropagation: () => {},
      target: a,
    };
    el.onClickLink(evt);
    el.addLinkElement();
    const evt2 = { ...evt };
    evt2.target = null;
    el.addLinkElement();
  });

  test('Test function editImage', () => {
    const img = document.createElement('img');
    img.style.width = '50px';
    img.style.height = '50px';
    const evt = {
      preventDefault: () => {},
      stopPropagation: () => {},
      target: img,
    };
    el.editImage(evt);
  });

  test('Test function verifyChange', () => {
    const img = document.createElement('img');
    const editor = el.shadowRoot.querySelector('main > .content-editor');
    editor.appendChild(img);
    el.verifyChange();
  });

  test('Test function okImage', () => {
    el.shadowRoot.querySelector('#w_image').value = '100px';
    el.shadowRoot.querySelector('#h_image').value = '100px';
    const img = document.createElement('img');
    img.style.width = '50px';
    img.style.height = '50px';
    const evt = {
      preventDefault: () => {},
      stopPropagation: () => {},
      target: img,
    };
    el.editImage(evt);
    el.okImage();
  });

  test('Test function removeImage', () => {
    const img = document.createElement('img');
    img.style.width = '50px';
    img.style.height = '50px';
    const evt = {
      preventDefault: () => {},
      stopPropagation: () => {},
      target: img,
    };
    el.editImage(evt);
    el.removeImage();
  });

  test('Test buttons commands', () => {
    const btns = el.shadowRoot.querySelectorAll('button');
    btns.forEach((btn) => {
      btn.click();
    });
  });
});
