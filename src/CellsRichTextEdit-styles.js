/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
  font-size: 16px;
  --bg-toolbar: #e9e9e9;
  --border-bottom-toolbar: #666;
  --color-icon: #333;
  --color-icon-active: #000;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

*,
*:before,
*:after {
  box-sizing: inherit;
}

main {
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
  margin: 0;
  padding: 0;
  position: relative;
}
main .toolbar {
  width: 100%;
  padding: 5px 10px 10px 10px;
  background-color: var(--bg-toolbar);
  border-bottom: 1px solid var(--border-bottom-toolbar);
  display: inline-block;
}
main .toolbar .color-input {
  width: 31px;
  margin: 0px 3px 0px 0px;
  margin-left: -2px;
  padding-left: 3px;
  padding-right: 3px;
  position: relative;
  z-index: 1;
  border: 1px solid #bdbdbd;
  background-color: white;
  border-left: none;
  outline: none;
}
main .toolbar .btn-input {
  margin-right: 0px;
}
main .toolbar .input-font-size {
  outline: none;
  margin: 0px 3px 0px 0px;
  position: relative;
  z-index: 1;
  width: 30px;
  margin-left: -3px;
  background-color: white;
  border: 1px solid #bdbdbd;
  border-left: none;
  padding-left: 6px;
}
main .toolbar .select-font-size {
  margin: 0px 3px 0px -3px;
  outline: none;
  position: relative;
  background-color: white;
  border: 1px solid #bdbdbd;
  border-left: none;
}
main .toolbar button {
  cursor: pointer;
  margin-top: 5px;
  margin-right: 3px;
  outline: none;
  background-color: white;
  border: 1px solid #bdbdbd;
  transition: all 0.2 ease-in-out;
  color: var(--color-icon);
}
main .toolbar button iron-icon {
  width: 20px;
}
main .toolbar button:last-child {
  margin-right: 0px;
}
main .toolbar button:hover {
  background-color: #f4f4f4;
  color: var(--color-icon-active);
}
main .bottom {
  border-bottom: none;
  border-top: 1px solid var(--border-bottom-toolbar);
  display: none;
}
main .content-editor {
  padding: 10px;
  width: 100%;
  outline: none;
  overflow: auto;
  border: 1px solid #bdbdbd;
}
main .dialog-link-wrapper {
  position: absolute;
  z-index: 10;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: rgba(255, 255, 255, 0.5);
  display: none;
  align-items: center;
  justify-content: center;
}
main .dialog-link-wrapper .dialog-link {
  width: 100%;
  margin: 0 2%;
  display: flex;
}
main .dialog-link-wrapper .dialog-link input {
  width: calc(100% - 40px);
  background-color: white;
  outline: none;
  padding: 10px 5px;
  border: 1px solid #BDBDBD;
}
main .dialog-link-wrapper .dialog-link button {
  width: 40px;
  outline: none;
  border: none;
  cursor: pointer;
  color: white;
}
main .dialog-link-wrapper .dialog-link .btn-add {
  background-color: #1464A5;
}
main .dialog-link-wrapper .dialog-link .btn-clear {
  background-color: #C0475E;
}
main .dialog-image-wrapper {
  position: absolute;
  z-index: 10;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: rgba(255, 255, 255, 0.5);
  display: none;
  align-items: center;
  justify-content: center;
}
main .dialog-image-wrapper .dialog-image {
  width: auto;
  margin: auto;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #072146;
  padding: 7px 10px;
}
main .dialog-image-wrapper .dialog-image label {
  font-size: 0.95em;
  margin-right: 5px;
  font-weight: 500;
  color: #fff;
}
main .dialog-image-wrapper .dialog-image input {
  width: 50px;
  background-color: white;
  border: 1px solid #bdbdbd;
  outline: none;
  padding: 5px;
  margin-right: 5px;
}
main .dialog-image-wrapper .dialog-image button {
  margin-left: 3px;
  border: none;
  padding: 2px 6px;
  color: white;
  cursor: pointer;
  outline: none;
}
main .dialog-image-wrapper .dialog-image .ok-image {
  margin-right: 3px;
  background-color: #006C6C;
}
main .dialog-image-wrapper .dialog-image .remove-image {
  background-color: #C0475E;
}
main .dialog-image-show {
  display: flex;
}
main .dialog-link-show {
  display: flex;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

input[type=number] {
  -moz-appearance: textfield;
}

@media only screen and (max-width: 480px) {
  main .toolbar {
    text-align: center;
  }
}
`;
