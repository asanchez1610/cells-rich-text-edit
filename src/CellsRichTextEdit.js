import { LitElement, html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsRichTextEdit-styles.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import '@cells-components/cells-icon/cells-icon.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/editor-icons.js';
import '@polymer/paper-tooltip/paper-tooltip.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-rich-text-edit></cells-rich-text-edit>
```

##styling-doc

@customElement cells-rich-text-edit
*/
let _linkSelected;
let _imageSelected;
const allCommands = [
  'bold',
  'italic',
  'underline',
  'color',
  'bgcolor',
  'parraf',
  'list',
  'listn',
  'left',
  'right',
  'justify',
  'center',
  'fontsize',
  'linkEl',
];
export class CellsRichTextEdit extends LitElement {
  static get is() {
    return 'cells-rich-text-edit';
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-rich-text-edit-shared-styles'),
    ];
  }

  // Declare properties
  static get properties() {
    return {
      maxHeight: {
        type: String,
        attribute: 'max-height',
      },
      minHeight: {
        type: String,
        attribute: 'min-height',
      },
      bold: Boolean,
      italic: Boolean,
      underline: Boolean,
      color: Boolean,
      bgcolor: Boolean,
      parraf: Boolean,
      list: Boolean,
      listn: Boolean,
      left: Boolean,
      right: Boolean,
      justify: Boolean,
      center: Boolean,
      fontsize: Boolean,
      linkEl: Boolean,
      indent: Boolean,
      outdent: Boolean,
      incluir: Array,
      excluir: Array,
      mode: String,
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.maxHeight = '350px';
    this.minHeight = '200px';
    this.mode = 'basic';
    this.excluir = [];
    this.incluir = [];
    this.initTextEdit();
  }

  async initTextEdit() {
    await this.updateComplete;
    if (this.mode === 'basic') {
      const itemsExclud = ['parraf', 'linkEl', 'justify'];
      this.managmenItems(itemsExclud, true);
    }
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      if (propName === 'incluir' && this.incluir.length > 0) {
        this.managmenItems(allCommands, true);
        this.managmenItems(this.incluir, false);
      } else if (propName === 'excluir' && this.excluir.length > 0) {
        this.managmenItems(allCommands, false);
        this.managmenItems(this.excluir, true);
      }
    });
  }

  managmenItems(items, hide) {
    items.forEach((item) => {
      this[item] = hide;
      this.requestUpdate();
    });
  }

  onClickLink(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    _linkSelected = evt.target;
    this.shadowRoot.querySelector('#input-link').value = evt.target.dataset.link
      ? evt.target.dataset.link
      : '';
    this.shadowRoot
      .querySelector('.dialog-link-wrapper')
      .classList.add('dialog-link-show');
  }

  verifyLinks() {
    const links = this.shadowRoot.querySelectorAll('a');
    if (links && links.length > 0) {
      links.forEach((linkElement) => {
        linkElement.setAttribute('target', '_blank');
        if (!linkElement.getAttribute('data-link')) {
          linkElement.setAttribute('data-link', '');
        }
        linkElement.removeEventListener(
          'dblclick',
          this.onClickLink.bind(this),
        );
        linkElement.addEventListener('dblclick', this.onClickLink.bind(this));
      });
    }
  }

  formatSelected(e, command, value) {
    e.preventDefault();
    if (command === 'createlink') {
      this.shadowRoot.ownerDocument.execCommand(
        command,
        false,
        ' Hipervinculo',
      );
    } else {
      this.shadowRoot.ownerDocument.execCommand(command, false, value);
    }
  }

  getContent() {
    return this.shadowRoot.querySelector('.content-editor').innerHTML;
  }

  contentHTML() {
    this.dispatchEvent(
      new CustomEvent('get-contet-rich-text-editor', {
        detail: this.shadowRoot.querySelector('.content-editor').innerHTML,
        bubbles: true,
        composed: true,
      }),
    );
  }

  setContent(content) {
    this.shadowRoot.querySelector('.content-editor').innerHTML = `${content}`;
  }

  colorInputByBrowser(comand, defaultColor, hide) {
    const esfirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    let style = 'height: 28px;top: 5px;';
    if (esfirefox) {
      style = 'height: 26px;top: 3px;padding:5px;';
    }
    return html`
      <input
        ?hidden="${hide}"
        class="color-input"
        type="color"
        id="favcolor"
        name="favcolor"
        .value="${defaultColor}"
        style="${style}"
        @change="${e => this.formatSelected(e, comand, e.target.value)}"
      />
    `;
  }

  fontSizeInputByBrowser() {
    const esfirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    let style = 'height: 28px;top:1px;';
    if (esfirefox) {
      style = 'height: 26px;top:1px;';
    }
    return html`
      <select
        ?hidden="${this.fontsize}"
        @change="${e => this.formatSelected(e, 'fontSize', e.target.value)}"
        class="select-font-size"
        style="${style}"
      >
        <option value="1" style="font-size:11px;">XS</option>
        <option value="2" style="font-size:13px;">Small</option>
        <option selected value="3" style="font-size:16px;">Normal</option>
        <option value="5" style="font-size:19px;">Medium</option>
        <option value="6" style="font-size:22px;">Large</option>
        <option value="7" style="font-size:25px;">XL</option>
      </select>
    `;
  }

  addLinkElement() {
    const linkElement = this.shadowRoot.querySelector('#input-link').value;
    if (linkElement && _linkSelected) {
      _linkSelected.setAttribute('href', linkElement);
      _linkSelected.setAttribute('data-link', linkElement);
    } else if (_linkSelected) {
      const tmp = this.shadowRoot.ownerDocument.createTextNode(
        _linkSelected.textContent,
      );
      _linkSelected.parentNode.replaceChild(tmp, _linkSelected);
    }
    this.shadowRoot
      .querySelector('.dialog-link-wrapper')
      .classList.remove('dialog-link-show');
    this.shadowRoot.querySelector('#input-link').value = '';
    _linkSelected = null;
  }

  editImage(evt) {
    _imageSelected = evt.target;
    if (_imageSelected.style) {
      if (_imageSelected.style.width) {
        this.shadowRoot.querySelector('#w_image').value = _imageSelected.style.width;
      }
      if (_imageSelected.style.height) {
        this.shadowRoot.querySelector('#h_image').value = _imageSelected.style.height;
      }
    }
    this.shadowRoot
      .querySelector('.dialog-image-wrapper')
      .classList.add('dialog-image-show');
  }

  verifyChange() {
    const images = this.shadowRoot.querySelectorAll('img');
    if (images && images.length > 0) {
      images.forEach((img) => {
        img.removeEventListener('dblclick', this.editImage.bind(this));
        img.addEventListener('dblclick', this.editImage.bind(this));
      });
    }

    this.verifyLinks();
  }

  okImage() {
    const width = this.shadowRoot.querySelector('#w_image').value;
    const height = this.shadowRoot.querySelector('#h_image').value;
    _imageSelected.removeAttribute('style');
    if (width) {
      _imageSelected.style.width = width;
    }
    if (height) {
      _imageSelected.style.height = height;
    }
    this.shadowRoot
      .querySelector('.dialog-image-wrapper')
      .classList.remove('dialog-image-show');
    _imageSelected = undefined;
    this.shadowRoot.querySelector('#w_image').value = '';
    this.shadowRoot.querySelector('#h_image').value = '';
  }

  removeImage() {
    if (_imageSelected) {
      _imageSelected.remove();
    }
    this.shadowRoot
      .querySelector('.dialog-image-wrapper')
      .classList.remove('dialog-image-show');
  }

  get buttonsToolbar() {
    return html` <button
        ?hidden="${this.bold}"
        id="btn-bold"
        @click="${e => this.formatSelected(e, 'bold')}"
      >
        <iron-icon icon="editor:format-bold"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-bold"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Negrita</paper-tooltip
      >

      <button
        ?hidden="${this.italic}"
        id="btn-italic"
        @click="${e => this.formatSelected(e, 'italic')}"
      >
        <iron-icon icon="editor:format-italic"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-italic"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Cursiva</paper-tooltip
      >

      <button
        ?hidden="${this.underline}"
        id="btn-underline"
        @click="${e => this.formatSelected(e, 'underline')}"
      >
        <iron-icon icon="editor:format-underlined"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-underline"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Subrayado</paper-tooltip
      >

      <button
        ?hidden="${this.color}"
        id="btn-color"
        class="btn-input"
        @click="${() => this.shadowRoot.querySelector('.color-input').click()}"
      >
        <iron-icon icon="editor:format-color-text"></iron-icon>
      </button>
      ${this.colorInputByBrowser('foreColor', '#000000', this.color)}
      <paper-tooltip
        for="btn-color"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Color de fuente</paper-tooltip
      >

      <button
        id="btn-bg"
        ?hidden="${this.bgcolor}"
        class="btn-input"
        @click="${() => this.shadowRoot.querySelector('.color-input').click()}"
      >
        <iron-icon icon="editor:format-color-fill"></iron-icon>
      </button>
      ${this.colorInputByBrowser('backColor', '#FFFFFF', this.bgcolor)}
      <paper-tooltip
        for="btn-bg"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Color de fondo</paper-tooltip
      >

      <button
        ?hidden="${this.parraf}"
        id="btn-parrafo"
        @click="${e => this.formatSelected(e, 'insertParagraph')}"
      >
        <iron-icon icon="editor:wrap-text"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-parrafo"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Insertar parrafo</paper-tooltip
      >

      <button
        ?hidden="${this.list}"
        id="btn-list"
        @click="${e => this.formatSelected(e, 'insertUnorderedList')}"
      >
        <iron-icon icon="editor:format-list-bulleted"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-list"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Lista simple</paper-tooltip
      >

      <button
        ?hidden="${this.listn}"
        id="btn-list-order"
        @click="${e => this.formatSelected(e, 'insertOrderedList')}"
      >
        <iron-icon icon="editor:format-list-numbered"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-list-order"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Lista ordenada</paper-tooltip
      >

      <button
        ?hidden="${this.left}"
        id="btn-text-left"
        @click="${e => this.formatSelected(e, 'justifyLeft')}"
      >
        <iron-icon icon="editor:format-align-left"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-text-left"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Aliniado a la izquierda</paper-tooltip
      >

      <button
        ?hidden="${this.right}"
        id="btn-text-right"
        @click="${e => this.formatSelected(e, 'justifyRight')}"
      >
        <iron-icon icon="editor:format-align-right"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-text-right"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Aliniado a la derecha</paper-tooltip
      >

      <button
        ?hidden="${this.center}"
        id="btn-text-center"
        @click="${e => this.formatSelected(e, 'justifyCenter')}"
      >
        <iron-icon icon="editor:format-align-center"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-text-center"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Aliniado al centro</paper-tooltip
      >

      <button
        ?hidden="${this.indent}"
        id="btn-text-inc"
        @click="${e => this.formatSelected(e, 'indent')}"
      >
        <iron-icon icon="editor:format-indent-increase"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-text-inc"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Aumentar sangria</paper-tooltip
      >

      <button
        ?hidden="${this.outdent}"
        id="btn-text-dec"
        @click="${e => this.formatSelected(e, 'outdent')}"
      >
        <iron-icon icon="editor:format-indent-decrease"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-text-dec"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Aumentar sangria</paper-tooltip
      >

      <button
        ?hidden="${this.justify}"
        id="btn-text-justify"
        @click="${e => this.formatSelected(e, 'justifyFull')}"
      >
        <iron-icon icon="editor:format-align-justify"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-text-justify"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Aliniación justificada</paper-tooltip
      >

      <button
        ?hidden="${this.fontsize}"
        id="btn-font-size"
        class="btn-input"
        @click="${e => this.formatSelected(e, 'fontSize', '+')}"
      >
        <iron-icon icon="editor:format-size"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-font-size"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Tamaño de letra</paper-tooltip
      >
      ${this.fontSizeInputByBrowser()}

      <button
        ?hidden="${this.linkEl}"
        id="btn-hipervinculo"
        @click="${e => this.formatSelected(e, 'createlink')}"
      >
        <iron-icon icon="editor:insert-link"></iron-icon>
      </button>
      <paper-tooltip
        for="btn-hipervinculo"
        position="bottom"
        animation-delay="0"
        offset="2"
        >Hipervinculo</paper-tooltip
      >`;
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <main>
        <div class="dialog-link-wrapper">
          <div class="dialog-link">
            <input id="input-link" />
            <button class="btn-add" @click="${this.addLinkElement}">
              <iron-icon icon="icons:check"></iron-icon>
            </button>
          </div>
        </div>

        <div class="dialog-image-wrapper">
          <div class="dialog-image">
            <label>W: </label> <input id="w_image" /> <label>H: </label>
            <input id="h_image" />
            <button @click="${this.okImage}" class="ok-image">
              <iron-icon icon="icons:check"></iron-icon>
            </button>
            <button @click="${this.removeImage}" class="remove-image">
              <iron-icon icon="icons:delete"></iron-icon>
            </button>
          </div>
        </div>

        <div class="toolbar" @click="${this.verifyChange}">
          ${this.buttonsToolbar}
        </div>

        <div
          @keyup="${this.verifyChange}"
          @paste="${this.verifyChange}"
          class="content-editor"
          contenteditable="true"
          autocorrect="off"
          autocapitalize="off"
          spellcheck="false"
          style="min-height:${this.minHeight};max-height:${this.maxHeight};"
        ></div>
      </main>
    `;
  }
}
