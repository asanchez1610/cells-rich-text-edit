import { setDocumentCustomStyles, } from '@bbva-web-components/bbva-core-lit-helpers';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }
  .content-buttons {
    display: flex;
    align-items: center;
    justify-content: center;
    width:100%;
    margin: 10px auto;
  }
`);
